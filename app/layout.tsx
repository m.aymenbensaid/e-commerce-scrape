import './globals.css'
import './util.css'
import './spinner.css'
import { Inter } from 'next/font/google'
import Link from 'next/link'

const inter = Inter({ subsets: ['latin'] })

export const metadata = {
  title: 'Lebsa Kids Scrapper',
  description: 'Lebsa Kids Scrapper',
}

export default function RootLayout({ children, }: { children: React.ReactNode }) {
  return (
    <html lang="en">
      <body className={inter.className}>

        <aside className='h-100'>
          <nav className='h-100 bg-dark pl-2 pr-2'>
            <ul>
              <li className='mb-3'><Link href="/">Scrape</Link></li>
              <li className='mb-3'><Link href="/show-input-file">Show Input File</Link></li>
              <li className='mb-3'><Link href="/upload">Upload File</Link></li>
              <li><Link href="/set-variant">Set Variant</Link></li>
            </ul>
          </nav>
        </aside>

        <main className='h-100 p-2'>{children}</main>

        <div id='box'></div>
      </body>
    </html>
  )
}
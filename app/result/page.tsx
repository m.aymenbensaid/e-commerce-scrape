'use client';

import { formScrapeAtom, isScrapingAtom, progressAtom } from "@/atoms";
import { useAtom } from "jotai";
import { useEffect } from "react";
import ScrapeInfo from '@/containers/ScrapeInfo';
import TableOutputProducts from '@/containers/TableOutputProducts';

export default function FormScrape() {
  const [state] = useAtom(formScrapeAtom);
  const [progress, setProgress]: any = useAtom(progressAtom);
  const [isScraping, setIsScraping]: any = useAtom(isScrapingAtom);

  useEffect(() => {
    let id: any;
    if (isScraping) {
      fetch('/scrape/progress');

      id = setInterval(async () => {
        const v = await fetch('/scrape/progress').then(v => v.json());
        const response = await fetch('/scrape/progress', { method: 'POST' }).then(v => v.json());

        setProgress({ ...v, message: response });

        if (v.done) {
          clearInterval(id);
          setIsScraping(false);
        }
      }, 5000);
    }

    return () => {
      clearInterval(id);
    }
  }, [isScraping]);

  return <>
    <div className='w-100 grid-2-1' style={{ maxHeight: '300px' }}>
      <div className='shadow p-1 mb-1 br7'>
        <div className='mb-1'>
          <label htmlFor="Service">Service</label>
          <input className='w-100' type="text" name="Service" id="Service" defaultValue={state.service} disabled />
        </div>

        <div className='mb-1'>
          <label htmlFor="inFile">Input File</label>
          <input className='w-100' type="text" name="inFile" id="inFile" defaultValue={state.inFile} disabled />
        </div>

        <div className='mb-1'>
          <label htmlFor="outFile">Output File</label>
          <input className='w-100' type="text" name="outFile" id="outFile" defaultValue={progress.outFile} disabled />
        </div>
      </div>

      <ScrapeInfo message={progress.message} />
    </div>

    <TableOutputProducts />
  </>
}

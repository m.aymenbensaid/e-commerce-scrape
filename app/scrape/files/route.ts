import ScrapeService from '@/services/ScrapeService';
import { NextResponse } from 'next/server';
// @ts-ignore: Unreachable code error
import * as Papa from 'convert-csv-to-json';

export async function GET(request: Request) {
  const url = new URL(request.url).search;
  const params = new URLSearchParams(url);
  const inDir = params.get('inDir');
  const allFiles = await ScrapeService.readDirctory(Boolean(JSON.parse(inDir!)));
  const files = allFiles.filter(f => f.endsWith('.csv') || f.endsWith('.xlsx'));
  return NextResponse.json(files);
}

export async function POST(request: Request) {
  const res: any = await request.json();
  const filePath = process.cwd() + '/_out/' + res.outFile + '.csv';

  let data = Papa.fieldDelimiter(',')
    .formatValueByType()
    .parseSubArray("*", ',')
    .supportQuotedField(true)
    .getJsonFromCsv(filePath);

  return NextResponse.json(data);
}
import ScrapeService from '@/services/ScrapeService';
import { FormScrape } from '@/types';
import { NextResponse } from 'next/server';

export async function GET(request: Request) {
  const url = new URL(request.url).search;
  const params = new URLSearchParams(url);
  let data = await ScrapeService.getDataFromCSV(params.get('inFile')!, params.get('service')!);
  return NextResponse.json(data);
}

export async function POST(request: Request) {
  const res: FormScrape = await request.json();
  ScrapeService.clearOutDir();
  ScrapeService.run(res);
  return NextResponse.json(res);
}
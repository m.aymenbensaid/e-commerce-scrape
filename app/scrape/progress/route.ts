import { BASE_PROGRESS_FILE_PATH, BASE_SCRAPE_INFO_FILE_PATH } from '@/utils/constants';
import { readFile } from 'fs/promises';
import { NextResponse } from 'next/server';

export async function GET(request: Request) {
  const data = await readFile(BASE_PROGRESS_FILE_PATH, { encoding: 'utf8' });
  return NextResponse.json(JSON.parse(data));
}

export async function POST(request: Request) {
  const data = await readFile(BASE_SCRAPE_INFO_FILE_PATH, { encoding: 'utf8' });
  return NextResponse.json(data);
}
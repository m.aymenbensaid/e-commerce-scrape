import ScrapeService from '@/services/ScrapeService';
import { BASE_IN_CSV_FILES_PATH } from '@/utils/constants';
import { writeFile } from 'fs/promises';
import { NextResponse } from 'next/server';

export async function GET(request: Request) {
  return NextResponse.json({ v: 'd' });
}

export async function POST(request: Request) {
  const res: any = await request.formData();

  const filename: string = res.get('filename');
  const blob: Blob = res.get('file');
  const buffer: ArrayBuffer = await blob.arrayBuffer();
  const ext = blob.type.includes('text/csv') ? '.csv' : '.xlsx';

  await writeFile(BASE_IN_CSV_FILES_PATH + '/' + filename + ext, Buffer.from(buffer));

  const files = await ScrapeService.readDirctory();
  return NextResponse.json({ name: filename, type: blob.type, size: blob.size, files });
}
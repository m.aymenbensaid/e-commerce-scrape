import { BASE_OUT_CSV_FILES_PATH } from '@/utils/constants';
import setVariant from '@/utils/setVariant';
import { NextResponse } from 'next/server';

export async function GET(request: Request) {
  const url = new URL(request.url).search;
  const params = new URLSearchParams(url);
  const inFile = params.get('inFile');

  const response = await setVariant(BASE_OUT_CSV_FILES_PATH + '/' + inFile);
  console.log(response);

  return NextResponse.json(response);
}

export async function POST(request: Request) {

}
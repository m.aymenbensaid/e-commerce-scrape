'use client';

import { useEffect, useState } from "react";

export default function FormScrape() {
  const [isLoading, setIsLoading] = useState(false);
  const [files, setFiles] = useState([])

  useEffect(() => {
    fetch('/scrape/files?inDir=false').then(r => r.json()).then(setFiles);
  }, []);

  const onSubmit = async (e: any) => {
    e.preventDefault();
    if (window.confirm('Confirm')) {
      setIsLoading(true);
      const inFile = e.target.elements[0].value;
      const response = await fetch('/scrape/variant?inFile=' + inFile).then(r => r.json());
      console.log(response);
      if(response) setIsLoading(false);
    }
  }

  return <>
    <form className="mb-2" onSubmit={onSubmit}>
      <div className='mb-1'>
        <label htmlFor="inFile">Input File</label>
        <select className='w-100' name="inFile" id='inFile'>
          {files.map((f, i) => <option key={i} value={f}>{f}</option>)}
        </select>
      </div>

      <button className='w-100 btn' type='submit' disabled={isLoading}>Show data</button>
    </form>
  </>
}

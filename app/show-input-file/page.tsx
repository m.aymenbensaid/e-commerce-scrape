'use client';

import { csvFileColumnsAtom, formScrapeAtom, inProductsAtom, inputFilesAtom } from "@/atoms";
import { useAtom } from "jotai";
import { useEffect, useState } from "react";

export default function FormScrape() {
  const [state, setData] = useAtom(formScrapeAtom);
  const [columns, setColumns] = useAtom(csvFileColumnsAtom);
  const [isLoading, setIsLoading] = useState(false);
  const [inProducts, setInProducts] = useAtom(inProductsAtom);
  const [inputFiles, setinputFiles] = useAtom(inputFilesAtom);

  useEffect(() => {
    fetch('/scrape/files?inDir=true').then(r => r.json()).then(setinputFiles);
  }, []);

  const onSubmit = (e: any) => {
    e.preventDefault();
    setIsLoading(true);

    const inFile = e.target.elements[0].value;

    fetch('/scrape/job?' + new URLSearchParams({ inFile, service: state.service }).toString())
      .then(r => r.json())
      .then((response) => {
        setColumns(response.columns);
        setInProducts(response.uniqProducts);
        setData({ ...state, ...response, inFile });
        setIsLoading(false);
      })
      .catch(e => {
        console.log(e);
        setIsLoading(false);
      });
  }

  return <>
    <form className="mb-2" onSubmit={onSubmit}>
      <div className='form-control'>
        <label htmlFor="inFile">Input File</label>
        <select className='w-100' name="inFile" id='inFile'>
          {inputFiles.map((f, i) => <option key={i} value={f}>{f}</option>)}
        </select>
      </div>

      <button className='w-100 btn' type='submit' disabled={isLoading}>Show data ({state.inFile})</button>
    </form>

    {columns.length > 0 && <div className="p-1 shadow br7 mb-2">
      <h4>"{state.inFile}" file contains ({inProducts.length}) items</h4>
      <div className='w-100 table d-flex' style={{ maxHeight: '500px' }}>
        <table className='w-100'>
          <thead>
            <tr>{columns && columns.map((k, i) => <th key={i}>{k}</th>)}</tr>
          </thead>
          <tbody>
            {inProducts.map((p, i) => <tr key={i + 'id'}>{columns.map((k, i) => <td key={i}>{p[k]}</td>)}</tr>)}
          </tbody>
        </table>
      </div>
    </div>}
  </>
}

import { atom } from "jotai";

export const formScrapeAtom = atom({
  service: 'mayoral',
  inFile: 'mayoral.csv',
  outFile: '',
  delay: 30,
  headless: true
});

export const inputFilesAtom = atom([]);

export const inProductsAtom = atom([]);

export const csvFileColumnsAtom = atom([]);

export const isLoadingAtom = atom(false);

export const isScrapingAtom = atom(false);

export const progressAtom = atom({ message: null, outFile: '', done: false, color: 'black' });
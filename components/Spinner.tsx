export default function Spinner({ children, inner = false, spin = true, bg = 'rgba(0, 0, 0, 0.5)' }: any) {
  return <div className={"spinner text-white text-uppercase" + (inner ? ' position-absolute' : '')}
    style={{ backgroundColor: bg }}>
    {spin && <div className="spinnerBorder mb-3" role="status"></div>}
    {children
      ? children
      : <p className="ltsp2">Chargement de données</p>}
  </div>;
}
import { useEffect, useRef } from "react";

const Li = ({ msg }: any) => {
  const inf = msg.split('|');
  return <li className={'mb-1 ml-1 ' + inf[1]}>{inf[0]}</li>
}

export default function ScrapeInfo ({ message }: any) {
  if (!message) return <></>;

  const msgRef :any= useRef();
  const messages = message.split(/\r\n|\n/g);

  useEffect(() => {
    if(msgRef && msgRef.current) msgRef.current.scrollTop = msgRef.current.scrollHeight;
  }, []);

  return <div className='w-100 p-1 bg-dark br7 shadow overflow'>
    <strong className='light'>Messages:</strong>
    <ul ref={msgRef} className='mt-1 overflow'>{messages.map((m: string, i: number) => <Li key={i} msg={m}></Li>)}</ul>
  </div>
}
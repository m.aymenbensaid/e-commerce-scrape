'use client';

import { formScrapeAtom, progressAtom } from '@/atoms';
import { useAtom } from 'jotai';
import React, { useState } from 'react'

export default function TableOutputProducts() {
  const [state] = useAtom(formScrapeAtom);
  const [progress] = useAtom(progressAtom);
  const [products, setProducts] = useState([]);

  const onShowOutputFileData = async () => {
    if (window.confirm('Confirm!')) {
      const response = await fetch('/scrape/files', { method: 'POST', body: JSON.stringify({ outFile: state.outFile, service: state.service }) });
      const json = await response.json();
      console.log(json);
      setProducts(json)
    }
  }

  if (!progress.done) return <></>

  return <div className='mt-1'>
    <button className='w-100 btn' onClick={onShowOutputFileData}>Show output file data</button>

    <div className='w-100 table d-flex' style={{ maxHeight: '300px' }}>
      {products.length > 0 && <table className='w-100 mt-1 overflow'>
        <thead>
          <tr>{Object.keys(products[0]).map((p, i) => <th key={i}>{p}</th>)}</tr>
        </thead>
        <tbody>
          {products.map((p, i) => <tr key={i + 'id'}>
            {Object.keys(products[0]).map((k, i) => <td title={p[k]} key={i}>{p[k]}</td>)}
          </tr>)}
        </tbody>
      </table>}
    </div>
  </div>
}

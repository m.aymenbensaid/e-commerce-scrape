'use client';

import { formScrapeAtom, inputFilesAtom, isScrapingAtom } from "@/atoms";
import { useAtom } from "jotai";
import { useRouter } from "next/navigation";
import { useEffect } from "react";

export default function FormScrape() {
  const [state, setData] = useAtom(formScrapeAtom);
  const [inputFiles, setinputFiles] = useAtom(inputFilesAtom);
  const [isScraping, setIsScraping]: any = useAtom(isScrapingAtom);

  const router = useRouter()

  useEffect(() => {
    fetch('/scrape/files?inDir=true').then(r => r.json()).then(setinputFiles);
  }, []);

  const onchange = (e: any) => {
    const value = e.target.name === "headless" ? JSON.parse(e.target.value) : e.target.value;
    setData({ ...state, [e.target.name]: value });
  }

  const onSubmit = async (e: any) => {
    e.preventDefault();

    const service: string = e.target.elements[0].value;
    const inFile: string = e.target.elements[1].value;
    const outFile: string = e.target.elements[4].value;

    if (window.confirm('Do you really want to scrape ' + service)) {
      const data = await fetch('/scrape/job', { method: 'POST', body: JSON.stringify({ ...state, service, inFile, outFile }) });
      const response = await data.json();
      setData({ ...state, service, inFile, outFile });
      setIsScraping(true);
      router.push('/result');
    }
  }

  return <form className="mb-2" onSubmit={onSubmit}>

    <div className="d-flex justify-between align-center shadow br7 p-2 mb-1">
      <div className='w-100'>
        <label htmlFor="service">Service</label>
        <select className='w-100' name="service" id="service" onChange={onchange} value={state.service}>
          <option value="mayoral">Mayoral</option>
          <option value="petit-bateau">Petit bateau</option>
          <option value="sergent-major">Sergent Major</option>
          <option value="kiabi">Kiabi</option>
        </select>
      </div>

      <div className='w-100 mr-1 ml-1'>
        <label htmlFor="inFile">Input File</label>
        <select className='w-100' name="inFile" id='inFile' onChange={onchange} value={state.inFile}>
          {inputFiles.map((f, i) => <option key={i} value={f}>{f}</option>)}
        </select>
      </div>

      <div className='w-100 mr-1'>
        <label htmlFor="headless">headless</label>
        <select className='w-100' name="headless" id="headless" onChange={onchange} value={'' + state.headless}>
          <option value="true">true</option>
          <option value="false">false</option>
        </select>
      </div>

      <div className='w-100 ml-1'>
        <label htmlFor="delay">Delay</label>
        <input className='w-100' type="number" name="delay" id="delay" onChange={onchange} value={state.delay} required />
      </div>
    </div>

    <div className="shadow br7 p-2 mb-1">
      <label htmlFor="outFile">Output file</label>
      <input className='w-100' type="text" name="outFile" id="outFile" onChange={onchange} value={state.outFile} placeholder='mayoral' required />
    </div>

    <div className="d-flex align-center">
      <button className='w-100 btn bg-red' type='submit' disabled={isScraping}>Start scrape ({state.inFile})</button>
    </div>
  </form>
}

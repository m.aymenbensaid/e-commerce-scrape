import { inputFilesAtom } from "@/atoms";
import { useAtom } from "jotai";
import { ChangeEvent, useState } from "react";

export default function FormUpload() {
  const [file, setFile] = useState<File>();
  const [isUploaded, setIsUploaded] = useState(true);
  const [_, setinputFiles] = useAtom(inputFilesAtom);

  const onFileUploadChange = (e: ChangeEvent<HTMLInputElement>) => {
    console.log("From onFileUploadChange");
    const file = e.target.files![0];
    setFile(file)
  };

  const onSubmit = async (e: any) => {
    e.preventDefault();
    setIsUploaded(false);

    const filename = e.target.elements[0].value;

    let formData = new FormData();
    formData.append("filename", filename);
    formData.append("file", file!);

    const res = await fetch("/scrape/upload", { method: "POST", body: formData, });
    const json = await res.json();
    if (json) {
      setinputFiles(json.files);
      setIsUploaded(true);
    }
  }

  return <div>
    <form className="shadow p-1 br7" action="/scrape/upload" onSubmit={onSubmit}>
      <div className="mb-1">
        <label htmlFor="filename">Filename</label>
        <input className="w-100" type="text" name="filename" id="filename" placeholder="mayoral" required />
      </div>

      <div className="mb-1">
        <label htmlFor="file">File Input</label>
        <input
          className="w-100"
          name="file"
          type="file"
          onChange={onFileUploadChange}
        />
      </div>

      <button className="btn" type="submit" disabled={!isUploaded}>upload</button>
    </form>
  </div>
}
import writeToProgressFile from '@/utils/writeToProgressFile';
import puppeteer, { Browser, Page } from 'puppeteer';
import { inProduct, Product, PuppeteerConfig } from "../types/index";

let browser: Browser;
let pages: Page[];
let page: Page;
const url = 'https://www.mayoral.com/fr/france';

export default async function MayoralService(config: PuppeteerConfig, inProduct: inProduct | null, counter: number, productsLen: number) {
  try {
    if (!inProduct) { throw new Error('End scraping') }

    if (!browser) {
      browser = await puppeteer.launch(config);
      await browser.newPage();
      pages = await browser.pages()
      page = pages[0];
    }

    if (!page || !inProduct) {
      await browser.close();
      process.exit(1);
    }
    if (inProduct.reference?.includes('/')) inProduct.reference = inProduct.reference.replace('/', '-');

    await page.goto(url, { waitUntil: 'networkidle2' });

    await page.evaluate(() => {
      if (document.querySelector('.cookie-banner__ctas button')) {
        (document.querySelector('.cookie-banner__ctas button') as HTMLButtonElement).click();
      }
    });

    const searchButton = await page.waitForSelector('#searchButton');
    searchButton!.evaluate((el: any) => el.click());

    await page.waitForSelector('#autoSuggest_result');
    await page.type('#autoSuggest_result input', inProduct.reference);

    const viewAllResults = await page.waitForSelector('#viewAllResults');
    viewAllResults!.evaluate((el: any) => el.click());

    await page.waitForSelector('.navbar');

    if (await page.evaluate(() => document.getElementById('showFacet') === null ? 0 : 1) === 1) {
      const message = `[Not Found] Product: ${inProduct?.reference} (${counter} / ${productsLen})`;
      writeToProgressFile({ message, outFile: config.outFile, color: 'red' });
      return [inProduct]
    }

    await page.waitForSelector('#product-carousel');
    await page.waitForSelector('.carousel-inner');

    const products = await page.evaluate(() => {
      const descriptionEl = document.querySelector('#product-description .contenido');
      let description = descriptionEl && descriptionEl.textContent && descriptionEl.textContent.length > 10
        ? descriptionEl.textContent
        : document.querySelector('#product-description .Eti')?.textContent;

      const categorie = document.getElementById('WC_BreadCrumb_Link_2')?.textContent?.trim().split(' ')[0];
      const nom = document.getElementById('InfoproductName')?.textContent?.trim();

      // @ts-ignore: Unreachable code error
      const images = [...document.querySelectorAll('.carousel-inner img')].map((el: any) => el.dataset.src || el.src) as string[];
      // @ts-ignore: Unreachable code error
      const tailles = [...document.querySelectorAll('.dropdown-menu.js-size-list a')].map(v => v.textContent?.trim()) as string[];
      const couleur = document.querySelector('.colors-carousel__item.active.selected')?.textContent as string;

      return tailles.filter(v => v).map(t => {
        const taille = t.trim().replace(/\n|\t|\t\n/g, '').replace(/\D+/g, '');
        return {
          nom,
          categorie,
          images: images.join('///'),
          taille: /mois/gi.test(t) ? taille + ' mois' : taille + ' ans',
          couleur: couleur.replace(/\s+|\n|\t|\t\n/g, '').trim(),
          description: description?.replace(/\s+|\n|\t|\t\n/g, ' ').trim()
        }
      })
    });

    const message = `[Done] Product: ${inProduct.reference} (${counter} / ${productsLen})`;
    writeToProgressFile({ message, outFile: config.outFile, color: 'green' });

    return (products as Product[]).map(p => {
      const ref = inProduct.reference.replace('-', '/');
      const reference = /ans/gi.test(p.taille) ? ref : ref + '-' + p.taille.replace(/Mois|ans/gi, '')
      return { ...inProduct, ...p, reference: reference.trim() }
    });

  } catch (error: any) {
    console.log('[Error] ', error.message);
    const message = `[Not Found] Product: ${inProduct?.reference} (${counter} / ${productsLen})`;
    writeToProgressFile({ message, outFile: config.outFile, color: 'red' });
    return [inProduct];
  }
}
import writeToProgressFile from '@/utils/writeToProgressFile';
import puppeteer, { Browser, Page } from 'puppeteer';
import { inProduct, PuppeteerConfig } from "../types/index";

let browser: Browser;
let pages: Page[];
let page: Page;
const url = 'https://www.petit-bateau.fr';
const url_tn = 'https://www.petit-bateau.tn';

export default async function PetitBateauService(config: PuppeteerConfig, inProduct: inProduct | null, counter: number, productsLen: number) {
  try {
    if (!browser) {
      browser = await puppeteer.launch(config);
      await browser.newPage();
      pages = await browser.pages()
      page = pages[0];
    }

    if (!page || !inProduct) {
      await browser.close();
      process.exit(1);
    }

    if (inProduct.reference?.includes('/')) inProduct.reference = inProduct.reference.replace('/', '-');

    await page.goto(url + '/recherche?q=' + inProduct.reference, { waitUntil: 'networkidle2' });
    await page.waitForSelector('#primary');

    const suggestions = await page.evaluate(() => {
      const suggestions = document.getElementById('search-result-items');
      if (document.querySelector('.description-wrapper ul') !== null) return 1;
      if (!suggestions || !document.querySelector('.product-search-count')) return 0;
      (suggestions?.querySelector('li .thumb-link') as HTMLAnchorElement).click();
      return suggestions?.children.length || 0;
    });

    if (suggestions < 1) {
      await page.goto(url_tn + '/instantsearch/result/?q=' + inProduct.reference, { waitUntil: 'networkidle2' });
      await page.waitForSelector('#maincontent');

      const suggestions = await page.evaluate(() => {
        const suggestions = document.querySelectorAll('.item.product.product-item');
        if (!suggestions || suggestions.length < 1) return 0;
        (suggestions[0]?.querySelector('a') as HTMLAnchorElement).click();
        return suggestions?.length || 0;
      });

      if (suggestions < 1) {
        const message = `[Not Found] Product: ${inProduct?.reference} (${counter} / ${productsLen})`;
        writeToProgressFile({ message, outFile: config.outFile, color: 'red' });
        return inProduct;
      }

      await page.waitForSelector('#glr_layout');

      const product = await page.evaluate(() => {
        // @ts-ignore: Unreachable code error
        const images = [...document.querySelectorAll('.item.gallery-item img')].map((el: any) => el.dataset.src || el.src) as string[];

        return {
          nom: document.querySelector('.product-name')?.textContent?.trim(),
          images: images.join('///'),
          description: document.getElementById('description')?.textContent?.replace(/\s+|\n|\t|\t\n/g, ' ').trim()
        }
      });

      const message = `[Done] Product: ${inProduct.reference} (${counter} / ${productsLen})`;
      await writeToProgressFile({ message, outFile: config.outFile, color: 'green' });
      return { ...inProduct, ...product };
    }
    else {
      await page.waitForSelector('#product-swiper-container');

      const product = await page.evaluate(() => {
        // @ts-ignore: Unreachable code error
        const images = [...document.querySelectorAll('.swiper-wrapper img')].map((el: any) => el.dataset.src || el.src) as string[];

        return {
          nom: document.querySelector('.product-name')?.textContent?.trim(),
          images: images.join('///'),
          description: document.querySelector('.description-wrapper ul')?.textContent?.replace(/\s+|\n|\t|\t\n/g, ' ').trim()
        }
      });

      const message = `[Done] Product: ${inProduct.reference} (${counter} / ${productsLen})`;
      await writeToProgressFile({ message, outFile: config.outFile, color: 'green' });
      return { ...inProduct, ...product };
    }
  } catch (error: any) {
    console.log('[Error] ', error.message);
    const message = `[Not Found] Product: ${inProduct?.reference} (${counter} / ${productsLen})`;
    writeToProgressFile({ message, outFile: config.outFile, color: 'red' });
    return inProduct;
  }
}

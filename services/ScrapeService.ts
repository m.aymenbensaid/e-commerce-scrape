import { inProduct, InputData, FormScrape, PuppeteerConfig } from "@/types";
import { BASE_IN_CSV_FILES_PATH, BASE_OUT_CSV_FILES_PATH, BASE_SCRAPE_INFO_FILE_PATH } from "@/utils/constants";
import { unlink, writeFile } from "fs/promises";
import { readdir } from "fs/promises";
import MayoralService from "./MayoralService";
import readXlsxFile from "read-excel-file/node";
import PetitBateauService from "./PetitBateauService";
import mergedArray from "@/utils/mergedArray";
import { json2csv } from "json-2-csv";
import SergentmajorService from "./SergentmajorService";
import config from '../utils/puppeteer';
// @ts-ignore: Unreachable code error
import * as Papa from 'convert-csv-to-json';
import colors from "@/utils/colors";
import writeToProgressFile from "@/utils/writeToProgressFile";

export default class ScrapeService {
  static async getColumnsFromCSV(inFile: string) {
    const data = await readXlsxFile(BASE_IN_CSV_FILES_PATH + '/' + inFile);
    return data[0];
  }

  static async readDirctory(inDir = true) {
    const files = await readdir(inDir ? BASE_IN_CSV_FILES_PATH : BASE_OUT_CSV_FILES_PATH);
    return files
  }

  static async clearOutDir() {
    const files = await readdir(BASE_OUT_CSV_FILES_PATH);
    files.forEach(file => {
      unlink(BASE_OUT_CSV_FILES_PATH + '/' + file);
    });
  }

  static async getDataFromCSV(inFile: string, service: string) {
    let products: inProduct[];
    let columns: string[];

    if (inFile.endsWith('csv')) {
      products = Papa.fieldDelimiter(',')
        .formatValueByType()
        .parseSubArray("*", ',')
        .supportQuotedField(true)
        .getJsonFromCsv(BASE_IN_CSV_FILES_PATH + '/' + inFile);
      columns = Object.keys(products[0]);
    }
    else {
      const data: any = await readXlsxFile(BASE_IN_CSV_FILES_PATH + '/' + inFile);
      columns = data[0].map((v: string) => v.toLowerCase());
      products = mergedArray(columns, data.filter((v: any) => v).slice(1));
    }

    const uniq = {};
    const uniqProducts = service && service === 'petit-bateau'
      ? products.slice(0)
      // @ts-ignore: Unreachable code error
      : products.filter(obj => !uniq[obj.reference] && (uniq[obj.reference] = true));

    console.log('\n---------------------------------------------------');
    console.log('> Total of products = ', products.length);
    console.log('> Total of uniq products = ', uniqProducts.length);
    console.log('> Columns = ', columns);
    console.log('---------------------------------------------------\n\n');

    return {
      products,
      uniqProducts,
      totalProducts: products.length,
      columns
    };
  }

  static async start(config: PuppeteerConfig, data: InputData, scrape: Function) {
    const { outFile, delay } = config;
    const { uniqProducts, products } = data;
    const productsLen = uniqProducts.length
    let outProducts: any = [];
    let counter = 0;
    const outFileCSVPath = `${BASE_OUT_CSV_FILES_PATH}/${outFile}.csv`;

    writeFile(BASE_SCRAPE_INFO_FILE_PATH, '');
    writeToProgressFile({ message: `[Start Scrape] ${config.inFile}`, color: 'yellow', outFile: outFileCSVPath, done: false });

    uniqProducts.forEach((inProduct: inProduct, index: number) => {
      let timeoutId = setTimeout(async () => {
        counter++;

        const response = await scrape(config, inProduct, counter, productsLen);

        if (response) {
          outProducts = Array.isArray(response) ? [...outProducts, ...response] : [...outProducts, response];
          writeFile(`${BASE_OUT_CSV_FILES_PATH}/${outFile}.json`, JSON.stringify(outProducts));
          clearTimeout(timeoutId);
        }

        if (counter >= productsLen) {
          console.log(`${colors.FgYellow}> All products are done: ${productsLen}${colors.Reset}`);
          scrape(config, null, counter, productsLen);
          const csv = await json2csv(outProducts);

          if (csv) {
            writeFile(`${BASE_OUT_CSV_FILES_PATH}/${outFile}.csv`, csv);
            clearTimeout(timeoutId);
            writeToProgressFile({
              message: `[End Scrape] All products are done (${counter} / ${productsLen}) for "${config.inFile}"`,
              outFile: outFileCSVPath,
              done: true,
              color: 'yellow'
            });
          }
        }
      }, 1000 * (index === 0 ? 0 : index * +delay));
    });
  }

  static async run(request: FormScrape) {
    const data: any = await ScrapeService.getDataFromCSV(request.inFile, request.service);
    const pconfig = { ...config, ...request };

    if (request.service === 'mayoral') this.start(pconfig, data, MayoralService);
    if (request.service === 'petit-bateau') this.start(pconfig, data, PetitBateauService);
    if (request.service === 'sergent-major') this.start(pconfig, data, SergentmajorService);
  }
}
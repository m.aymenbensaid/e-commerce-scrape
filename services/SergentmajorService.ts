import writeToProgressFile from '@/utils/writeToProgressFile';
import puppeteer, { Browser, Page } from 'puppeteer';
import { inProduct, PuppeteerConfig } from "../types/index";

let browser: Browser;
let pages: Page[];
let page: Page;
const url = 'https://www.sergent-major.com';

export default async function SergentmajorService(config: PuppeteerConfig, inProduct: inProduct | null, counter: number, productsLen: number) {
  try {
    if (!browser) {
      browser = await puppeteer.launch(config);
      await browser.newPage();
      pages = await browser.pages()
      page = pages[0];
    }

    if (!page || !inProduct) {
      await browser.close();
      process.exit(1);
    }

    if (inProduct.reference?.includes('-')) inProduct.reference = inProduct.reference.split('-')[0];

    await page.goto(url + '/recherche?q=' + inProduct.reference, { waitUntil: 'networkidle2' });
    await page.waitForSelector('#gtmProductCount');

    await page.evaluate(() => {
      document.body.classList.remove('tc-modal-open');
      const overlay = document.getElementById('privacy-overlay');
      const popin_tc_privacy = document.getElementById('privacy-overlay');
      const popin_tc_privacy_container_text = document.getElementById('popin_tc_privacy_container_text-overlay');
      if (popin_tc_privacy_container_text) popin_tc_privacy_container_text?.style.setProperty('display', 'none!important');
      if (popin_tc_privacy) popin_tc_privacy?.style.setProperty('display', 'none!important');

      if (overlay) {
        overlay!.style.display = 'none';
        overlay!.style.visibility = 'hidden';
      }
    });

    await page.waitForSelector('#product-search-results');

    const suggestions = await page.evaluate((reference) => {
      const suggestions = document.querySelectorAll('.product-tile-container');
      // @ts-ignore: Unreachable code error
      const listProducts = [...document.querySelectorAll('.product-tile-container')];

      if (!suggestions || suggestions.length < 1 || listProducts.length < 1) return 0;

      const url = new URL(window.location.href);
      reference = reference || '' + new URLSearchParams(url.search).get('q');

      const isFound: any = listProducts.find((el: any) => {
        const child = el.querySelector('.product');
        if (!child) return null;
        return new RegExp(reference, 'gi').test(child?.dataset?.pid) === true ? el : null;
      });

      // console.log('isFound ===> ',isFound);

      if (!isFound || !isFound.querySelector("a.product-image-link")) return 0;

      (isFound.querySelector("a.product-image-link") as HTMLAnchorElement)!.click();
      return listProducts?.length || 0;
    }, inProduct.reference);

    if (suggestions < 1) {
      const message = `[Not Found] Product: ${inProduct?.reference} (${counter} / ${productsLen})`;
      writeToProgressFile({ message, outFile: config.outFile, color: 'red' });
      return null;
    }

    await page.waitForSelector('.primary-images');

    const product = await page.evaluate(() => {
      // @ts-ignore: Unreachable code error
      const images = [...document.querySelectorAll('.primary-images img')].map((el: any) => el.dataset.src || el.src) as string[];
      const description = document.querySelector('.product-description')?.textContent?.replace(/\s+|\n|\t|\t\n/g, ' ').trim();
      const descriptionli = [...document.querySelectorAll('#product-description li')].map(v => v.textContent?.trim());

      return {
        nom: document.querySelector('.product-name')?.textContent?.trim(),
        images: images.join('///'),
        description: description && description.length > 5 ? description + '\n' + descriptionli : descriptionli
      }
    });

    const message = `[Done] Product: ${inProduct.reference} (${counter} / ${productsLen})`;
    writeToProgressFile({ message, outFile: config.outFile, color: 'green' });

    return { ...inProduct, ...product };
  } catch (error: any) {
    console.log('[Error] ', error.message);
    const message = `[Not Found] Product: ${inProduct?.reference} (${counter} / ${productsLen})`;
    writeToProgressFile({ message, outFile: config.outFile, color: 'red' });
    return null;
  }
}

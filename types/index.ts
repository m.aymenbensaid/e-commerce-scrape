export type inProduct = {
  id?: number
  reference: string
  prix: number
}

export type Product = {
  nom: string | undefined;
  categorie: string | undefined;
  images: string;
  taille: string;
  couleur: string;
  description: string | undefined;
  id?: number | undefined;
  reference: string;
  prix: number;
}

export type FormScrape = {
  service: string
  inFile: string
  outFile: string
  delay: number
  headless: boolean
}

export type InputData = {
  products: [],
  uniqProducts: [],
  totalProducts: number,
  columns: string[]
}

export type PuppeteerConfig = {
  headless: boolean
  devtools: boolean
  defaultViewport: { width: 1366, height: 768 },
  args: string[]
  ignoreDefaultArgs?: string[]
  service: string
  inFile: string
  outFile: string
  delay: number
}
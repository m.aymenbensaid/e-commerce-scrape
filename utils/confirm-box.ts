export default function ConfirmBox(message: string, title: string) {
  const isBrowser = typeof window !== "undefined";
  let isDeleted = false;
  let isConfirmed = false;

  if (!isBrowser) return;

  return new Promise(resolve => {

    const body = document.body;
    const div = document.createElement('div');
    div.id = 'confirm-box';

    const divInner = document.createElement('div');
    const divBtn = document.createElement('div');
    const btnConfirm = document.createElement('button');
    const btnCancel = document.createElement('button');

    btnConfirm.onclick = () => {
      isConfirmed = true;
      body.removeChild(div);
      isDeleted = true;
      resolve(isConfirmed)
    }

    btnCancel.onclick = () => {
      isConfirmed = false;
      body.removeChild(div);
      isDeleted = true;
      resolve(isConfirmed)
    }

    const content = `<div class="mb-3">
      <h4><i class="fa fa-info-circle mr-2 mb-2"></i>${title}</h4>    
      <p>${message}</p>
    </div>`;

    divInner.innerHTML = content;

    divInner.classList.add('scale');
    divBtn.classList.add('d-flex', 'justify-end')
    btnCancel.classList.add('btn', 'bg-red', 'mr-2');
    btnConfirm.classList.add('btn')
    btnConfirm.textContent = 'Confirmer';
    btnCancel.textContent = 'Annuler';

    divBtn.appendChild(btnCancel);
    divBtn.appendChild(btnConfirm);
    divInner.appendChild(divBtn);
    div.appendChild(divInner)
    body.appendChild(div);
  });
}

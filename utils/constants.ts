export const BASE_IN_CSV_FILES_PATH = process.cwd() + '/_in';
export const BASE_OUT_CSV_FILES_PATH = process.cwd() + '/_out';
export const BASE_PROGRESS_FILE_PATH = process.cwd() + '/_progress.json';
export const BASE_SCRAPE_INFO_FILE_PATH = process.cwd() + '/_scrape_info';
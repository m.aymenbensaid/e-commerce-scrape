/**
const columns = ['id', 'reference'];
const data = [[1, '54687'], [2, '687'], [3, 546]];


[
  { id: 1, reference: '54687' },
  { id: 2, reference: '687' },
  { id: 3, reference: '546' }
]
 */

const mergedArray = (columns: any, data: any) => {
  return data.map((row: any) => {
    const mergedObject: any = {};
    columns.forEach((column: any, index: number) => {
      mergedObject[column] = /reference/gi.test(column) ? '' + row[index] : row[index];
    });
    return mergedObject;
  });
}

export default mergedArray
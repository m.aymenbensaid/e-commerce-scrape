import { PuppeteerConfig } from "@/types";

//const adBlock = `C:\\Users\\haike\\AppData\\Local\\Google\\Chrome\\User Data\\Default\\Extensions\\cjpalhdlnbpafiamejdnhcphjbkeiagm\\1.41.2_1`
const adBlock = '/home/haikel/.config/google-chrome/Default/Extensions/cjpalhdlnbpafiamejdnhcphjbkeiagm/1.49.2_0';

const config: PuppeteerConfig = {
  headless: true,
  devtools: false,
  defaultViewport: { width: 1366, height: 768 },
  args: [
    "--disable-infobars",
    "--start-maximized",
    // "--test-type",
    // '--disable-extensions-except=' + adBlock,
    // '--load-extension=' + adBlock
  ],
  // ignoreDefaultArgs: ["--enable-automation"]
}

export default config;
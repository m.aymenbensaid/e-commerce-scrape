import readXlsxFile from "read-excel-file/node";
import { writeFile } from 'fs/promises';
import { nanoid } from 'nanoid'
import { json2csv } from "json-2-csv";

const mergedArray = (columns: string, res: any) => {
  console.log(columns, res);
  
  return res.map((item: any) => {
    const obj = {};
    for (let i = 0; i < columns.length; i++) {
      // @ts-ignore: Unreachable code error
      obj[columns[i]] = item[i];
    }
    return obj;
  });
}

export default async function setVariant(filePath: string) {
  const fileContent = await readXlsxFile(filePath);
  const colunms = fileContent[0];
  const data = fileContent.slice(1);
  let result: any = [];

  data.forEach((d: any, i: number) => {
    const reference = d[1].split('-')[0];
    const variant = nanoid().slice(0, 6);
    const regex = new RegExp(reference, 'gi');

    if (!result.find((k: any) => regex.test(k))) {
      d[0] = variant;
      result = [...result, d];
    }
    else {
      const founded = result.find((v: any) => regex.test(v));
      d[0] = founded[0];
      result = [...result, d];
    }
  });

  // @ts-ignore: Unreachable code error
  const csv = await json2csv(mergedArray(colunms, result));
  
  if(csv) writeFile(filePath, csv);
  // writeFile('output.json', JSON.stringify(mergedArray(colunms, result)));
  console.log(result.length)
  // console.log(result);
  return true;
}
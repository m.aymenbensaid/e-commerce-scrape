import { readFile, writeFile } from "fs/promises";
import colors from "./colors";
import { BASE_OUT_CSV_FILES_PATH, BASE_PROGRESS_FILE_PATH, BASE_SCRAPE_INFO_FILE_PATH } from "./constants";

type Progress = { message: string, outFile: string, color?: string, done?: boolean }

export default async function writeToProgressFile({ message, outFile, color, done = false }: Progress) {
  const c = 'Fg' + color?.slice(0, 1).toUpperCase() + color?.slice(1);
  // @ts-ignore: Unreachable code error
  console.log(`${colors[c]} ${message} ${colors.Reset}`);

  const fileContent = await readFile(BASE_SCRAPE_INFO_FILE_PATH, { encoding: 'utf8' });
  writeFile(BASE_SCRAPE_INFO_FILE_PATH, fileContent + '\n' + message + '|' + color);
  const outFileCSVPath = `${BASE_OUT_CSV_FILES_PATH}/${outFile}.csv`;
  writeFile(BASE_PROGRESS_FILE_PATH, JSON.stringify({ message, outFile: outFileCSVPath, color, done }));
}